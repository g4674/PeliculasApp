
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { HomeScreen } from '../screens/HomeScreen';
import { DetailScreen } from '../screens/DetailScreen';
import { Movie } from '../interfaces/movieInterface';
import { LoginScreen } from '../screens/LoginScreen';
import { SingupScreen } from '../screens/SingupScreen';
import { PermissionsScreen } from '../screens/PermissionsScreen';

export type RootStackParams = {
  HomeScreen: undefined;
  DetailScreen: Movie;
  LoginScreen: undefined;
  SingupScreen: undefined;
}

const Stack = createStackNavigator<RootStackParams>();


export const Navigation = () => {
  return (
    
    //<Stack.Navigator initialRouteName='LoginScreen'>
    <Stack.Navigator initialRouteName='LoginScreen'>
      <Stack.Screen name="LoginScreen" component={ LoginScreen } />
      <Stack.Screen name="HomeScreen" component={ HomeScreen } />
      <Stack.Screen name="DetailScreen" component={ DetailScreen } />
      <Stack.Screen name="SingupScreen" component={ SingupScreen } />
    </Stack.Navigator>
  );
}