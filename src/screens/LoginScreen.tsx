import React, { useState } from 'react'
import { Alert, Button, Image, StyleSheet, Text, View, Platform } from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import { Movie } from '../interfaces/movieInterface';
import { useNavigation } from '@react-navigation/core';
import { check, PERMISSIONS, PermissionStatus, request } from 'react-native-permissions'

export const LoginScreen = (Navigation: any) => {
    const navigation = useNavigation();
    const [nom, setNom] = useState('');
    const [con, setContrasena] = useState('');
    

        const checkLocationPermission = async () => {

            let permissionStatus: PermissionStatus; 
    
            if( Platform.OS === 'ios') {
                permissionStatus = await request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);
                
            }else{
                permissionStatus = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
    
            }
    
            console.log({ permissionStatus });

            verificarsesion();
    
        }

    function verificarsesion() {
   
        {
          fetch("https://josuejaime.com/Usuarios/iniciarsesion/"+nom+"/"+con)
          .then(response=>response.json())
          .then(respJson=>{
            if(respJson.resultado===true)
            {
            Alert.alert(
              'Sesion',
              respJson.mensaje,
              [
                //onPress={ () => navigation.navigate('HomeScreen' ) }
                {text: 'Siguiente', onPress: () => {navigation.navigate('HomeScreen',{usuario:nom,pass:con})}},
                
              ],
              { cancelable: false }
            )
            }else{
              Alert.alert(
                'Sesion',
                'Usuario y/o Contraseña incorrecta \nVerifique sus datos',
                [
                  {text: 'OK', onPress: () => {setNom('');setContrasena('')}},
                ],
                { cancelable: false }
              )
            }
          })
      }
     
      }


  return (
    <View>
        <Text style={{ fontSize: 30, fontWeight: 'bold', marginLeft: 10, textAlign: 'center' }}>Iniciar Sesion</Text>
        <Image
            source={require('../img/login.png')}
            style={{
            width: '100%',
            height: 150,
            resizeMode: 'center',
            }}
        />
        <View>
            <TextInput
                style={styles.TextInput}
                placeholder='Usuario'
                keyboardType='default'
                value={nom}
                onChangeText={texto =>
                setNom(texto)
                } /*Asignamos el valor del texto y usamos set para escribir en  la variable*/
                //style={{fontSize:30,marginBottom:10,alignSelf:"center",marginTop:15,borderWidth:1,padding:10}}
                />
        </View>
        <View>
            <TextInput
                style={styles.TextInput}
                placeholder='Contraseña'
                keyboardType='default'
                secureTextEntry={true}
                value={con}
                onChangeText={texto =>
                setContrasena(texto)
                } /*Asignamos el valor del texto y usamos set para escribir en  la variable*/
                //style={{fontSize:30,alignSelf:"center",borderWidth:1,padding:5}}
            />
        </View>
        
        <Button
            title='Entrar'
            onPress={checkLocationPermission}
        />
        <Text></Text>
        <Button
            color="#D2B4DE"
            title='Registrarse'
           // onPress={ () => navigation.navigate('SingupScreen') }
            onPress={ () => {navigation.navigate('SingupScreen' )} }
            
        />
        
        {/* <Button
            title='Permiso'
            onPress={checkLocationPermission}
        /> */}
        
        
    </View>
    
    
    
  )
}


const styles = StyleSheet.create({
    permisos: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
        TextInput: {
        borderRadius: 5,
        borderColor: '#333',
        paddingHorizontal: 10,
        paddingVertical: 15,
        fontSize: 16,
        borderWidth: 1,
        marginHorizontal: 5,
        marginVertical: 10
    },



});