import { useNavigation } from '@react-navigation/native';
import React, { useState } from 'react'
import { Alert, Button, Image, StyleSheet, Text, View } from 'react-native'
import { TextInput } from 'react-native-gesture-handler';

export const SingupScreen = () => {
    const navigation = useNavigation();
    const [nom, setNom] = useState('');
    const [con, setContrasena] = useState('');
    const [conC, setContrasenaC] = useState('');

    function registrar() {
   
        {
          fetch("https://josuejaime.com/Usuarios/crearusuario/"+nom+"/"+con)
          .then(response=>response.json())
          .then(respJson=>{
            if(respJson.mensaje==='')
            {
            Alert.alert(
              'Sesion',
              respJson.mensaje,
              [
                //onPress={ () => navigation.navigate('HomeScreen' ) }
                {text: 'Siguiente', onPress: () => {navigation.navigate('LoginScreen',{usuario:nom,pass:con})}},
                
              ],
              { cancelable: false }
            )
            }else{
              Alert.alert(
                'Sesion',
                'Error de conexion',
                [
                  {text: 'OK', onPress: () => {setNom('');setContrasena('');setContrasenaC('')}},
                ],
                { cancelable: false }
              )
            }
          })
      }
     
    }

  return (
    <View>
        <Text style={{ fontSize: 30, fontWeight: 'bold', marginLeft: 10, textAlign: 'center' }}>Registrarse</Text>
        <Image
            source={require('../img/login.png')}
            style={{
            width: '100%',
            height: 150,
            resizeMode: 'center',
            }}
        />
        <View>
            <TextInput
                style={styles.TextInput}
                placeholder='Usuario'
                keyboardType='default'
                value={nom}
                onChangeText={texto =>
                setNom(texto)
                } /*Asignamos el valor del texto y usamos set para escribir en  la variable*/
                //style={{fontSize:30,marginBottom:10,alignSelf:"center",marginTop:15,borderWidth:1,padding:10}}
            />
        </View>
        <View>
            <TextInput
                style={styles.TextInput}
                placeholder='Contraseña'
                keyboardType='default'
                secureTextEntry={true}
                value={con}
                onChangeText={texto =>
                setContrasena(texto)
                } /*Asignamos el valor del texto y usamos set para escribir en  la variable*/
                //style={{fontSize:30,alignSelf:"center",borderWidth:1,padding:5}}
            />
        </View>
        <View>
            <TextInput
                style={styles.TextInput}
                placeholder='Confirma contraseña'
                keyboardType='default'
                secureTextEntry={true}
                value={conC}
                onChangeText={texto =>
                setContrasenaC(texto)
                } /*Asignamos el valor del texto y usamos set para escribir en  la variable*/
                //style={{fontSize:30,alignSelf:"center",borderWidth:1,padding:5}}
            />
        </View>
        <Text></Text>
        <Button
            color='#D2B4DE'
            title='Registrarse'
            onPress={registrar}
        />
    </View>
    
    
  )
}

const styles = StyleSheet.create({
    permisos: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
        TextInput: {
        borderRadius: 5,
        borderColor: '#333',
        paddingHorizontal: 10,
        paddingVertical: 15,
        fontSize: 16,
        borderWidth: 1,
        marginHorizontal: 5,
        marginVertical: 10
    },



});
